/*
 * Compilación: g++ servidor.cpp -o servidor
 * Uso: ./servidor <puerto>
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <limits.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <thread>

using namespace std;

const int BOARD_SIZE = 15;
int port;
vector<thread> clientSockets;

class Ship {
    public:
        Ship();

        void setIdentifier(char identifier); // Identificador de la embarcación (P, B, S, L)
        void addPosition(int x, int y); // Agrega un punto de la embarcación
        void clearPositions(); // Elimina todos los puntos de la embarcación

        char getIdentifier(); // Obtiene el identificador de la embarcación
        int getPoints(); // Obtiene los puntos de la embarcación
        vector<pair<int, int>> getPositions(); // Obtiene las posiciones de la embarcación

    private:
        char identifier;
        int points;
        vector<pair<int, int>> positions;
};

// ---- Ship Constructor ----
Ship::Ship() {
    identifier = 0;
    points = 0;
    positions = vector<pair<int, int>>();
}

// ---- Ship Setters ----
void Ship::setIdentifier(char identifier) {
    this->identifier = identifier;
    switch (identifier) {
        case 'P':
            this->points = 5;
            break;
        case 'B':
            this->points = 4;
            break;
        case 'S':
            this->points = 3;
            break;
        case 'L':
            this->points = 1;
            break;
    }
}


// ---- Ship Functions ----
void Ship::addPosition(int x, int y) {
    positions.push_back(make_pair(x, y));
}

void Ship::clearPositions() {
    positions.clear();
}

// ---- Ship Getters ----
char Ship::getIdentifier() {
    return identifier;
}

int Ship::getPoints() {
    return points;
}

vector<pair<int, int>> Ship::getPositions() {
    return positions;
}

class Board {
    public:
        Board();

        void setShips(); // Agrega las embarcaciones
        void removeShip(Ship ship); // Elimina la embarcación
        void drawBoard(bool own); // Imprime la matriz de embarcaciones

        vector<Ship> getShips();

    private:
        vector<Ship> ships;
        char board[BOARD_SIZE][BOARD_SIZE];
};

// ---- Board Constructor ----
Board::Board() {
    /*    */
}

// ---- Board Setters ----
void Board::setShips() {
    // Rellena todo con caracteres vacíos
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j] = ' ';
        }
    }

    int total_p = 1; // Total de portaaviones
    int total_b = 2; // Total de buques
    int total_s = 2; // Total de submarinos
    int total_l = 3; // Total de lanchas
    int total = total_p + total_b + total_s + total_l; // Total de embarcaciones


    srand(time(nullptr));
    for (int i = 0; i < total; i++) {
        Ship ship;

        // Identifico la embarcación que falta por agregar al tablero
        if (total_p > 0) {
            ship.setIdentifier('P');
            total_p--;
        } else if (total_b > 0) {
            ship.setIdentifier('B');
            total_b--;
        } else if (total_s > 0) {
            ship.setIdentifier('S');
            total_s--;
        } else if (total_l > 0) {
            ship.setIdentifier('L');
            total_l--;
        }

        // Asigno las posiciones de la embarcación
        bool added = false;
        do {
            // Vacío las posiciones de la embarcación
            ship.clearPositions();

            // Posición aleatoria
            pair<int, int> random_pos = make_pair(rand() % BOARD_SIZE, rand() % BOARD_SIZE);
            if (board[random_pos.first][random_pos.second] != ' ') continue;
            ship.addPosition(random_pos.first, random_pos.second);

            // Dirección
            int direction = rand() % 4; // 0: izquierda, 1: arriba, 2: derecha, 3: abajo

            for (int count = 1; count <= ship.getPoints(); count++) {
                pair<int, int> next_pos;
                switch (direction) {
                    case 0: // Mantiene el eje X
                        next_pos = make_pair(random_pos.first, random_pos.second - count);
                        break;
                    case 1: // Mantiene el eje Y
                        next_pos = make_pair(random_pos.first - count, random_pos.second);
                        break;
                    case 2: // Mantiene el eje X
                        next_pos = make_pair(random_pos.first, random_pos.second + count);
                        break;
                    case 3: // Mantiene el eje Y
                        next_pos = make_pair(random_pos.first + count, random_pos.second);
                        break;
                }

                // Si colisiona con otra embarcación, se repite el proceso
                if (board[next_pos.first][next_pos.second] != ' '
                    || next_pos.first < 0 || next_pos.first > BOARD_SIZE - 1
                    || next_pos.second < 0 || next_pos.second > BOARD_SIZE - 1) {
                    added = false;
                    break;
                } else {
                    ship.addPosition(next_pos.first, next_pos.second);
                    added = true;
                }
            }
        } while (added == false);

        // Agrega la embarcación al tablero
        for (int i = 0; i < ship.getPoints(); i++) {
            board[ship.getPositions()[i].first][ship.getPositions()[i].second] = ship.getIdentifier();
        }
    }
}

// ---- Board Methods ----
void Board::removeShip(Ship ship) {
    for (int i = 0; i < static_cast<int>(ships.size()); i++) {
        if (ships[i].getPoints() == 0) {
            ships.erase(ships.begin() + i);
        }
    }
}

void Board::drawBoard(bool own) {
    if (own)
        cout << "            TABLERO MIS EMBARCACIONES\n\n";
    else
        cout << "TABLERO DISPAROS\n\n";

    for (int i = 0; i < BOARD_SIZE; i++) {
        cout << static_cast<char>('A' + i) << " ";
        for (int j = 0; j < BOARD_SIZE; j++) {
            // own = true -> muestra mi tablero | false -> muestra tablero enemigo
            switch (board[i][j]) {
                case 'P':
                    cout << (own ? "[P]" : "[ ]");
                    break;
                case 'B':
                    cout << (own ? "[B]" : "[ ]");
                    break;
                case 'S':
                    cout << (own ? "[S]" : "[ ]");
                    break;
                case 'L':
                    cout << (own ? "[L]" : "[ ]");
                    break;
                case 'X':
                    cout << "[X]";
                    break;
                default:
                    cout << "[ ]";
            }
        }
        cout << "\n";
    }
    cout << "   1  2  3  4  5  6  7  8  9  10 11 12 13 14 15\n";
}

// ---- Board Getters ----
vector<Ship> Board::getShips() {
    return ships;
}

// ---- Global Functions ----
bool checkPort(char *argv[], int &port) {
    char *p;
    errno = 0;

    long conv = strtol(argv[1], &p, 10);

    /* Valida que el puerto sea un entero válido */
    if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < INT_MIN) {
        return false;
    } else {
        port = conv;
        return port >= 1000 && port <= 9999; // Valida que el puerto esté entre 1000 y 54000
    }
}

int main(int argc, char *argv[]) {

    // Validar que se ingrese sólo el puerto
    if (argc == 2) {
        // Validar que el puerto sea válido
        if (!checkPort(argv, port)) {
            cerr << "Puerto incorrecto.\nEl puerto tiene que estar entre 1000 y 9999." << endl;
            return 1;
        }
    } else {
        cerr << "Número de parámetros incorrecto. Se requiere sólo el puerto." << endl;
        return 1;
    }

    // Creación del socket
    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket < 0) {
        cerr << "Error al crear el socket." << endl;
        return 1;
    }

    // Establece la información de dirección del socket
    struct sockaddr_in address;
    address.sin_family = AF_INET; // IPv4
	address.sin_addr.s_addr = INADDR_ANY; // Escucha en todas las interfaces de red
    address.sin_port = htons(port); // Establece el puerto
    inet_pton(AF_INET, "0.0.0.0", &address.sin_addr);

    // Enlaza el socket a una dirección IP y puerto específico
    if (bind(serverSocket, (struct sockaddr*) &address, sizeof(address)) < 0) {
        cerr << "Error al enlazar el socket." << endl;
        return 1;
    }

    // Establce la cantidad de conexiones entrantes pendientes en la cola a 4096
    if (listen(serverSocket, SOMAXCONN) == 0) {
        cout << "Esperando conexiones...\n" << endl;
    }

    vector<int> clientSockets;

    while (true) {
        // Aceptar una nueva conexión de cliente
        struct sockaddr_in clientAddr;
        socklen_t clientAddrLen = sizeof(clientAddr);
        int newClientSocket = accept(serverSocket, (struct sockaddr *)&clientAddr, &clientAddrLen);
        
        if (newClientSocket < 0) {
            cerr << "Error aceptando la conexión." << endl;
            continue;
        }

        // Add the new client socket to the vector
        clientSockets.push_back(newClientSocket);

        // Create a new thread to handle the client
        thread clientThread([&]() {
            // Get client IP and port
            char clientIP[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &(clientAddr.sin_addr), clientIP, INET_ADDRSTRLEN);
            int clientPort = ntohs(clientAddr.sin_port);
            cout << "Juego nuevo[" << clientIP << ":" << clientPort << "]" << endl;

            Board board;
            board.setShips();
            board.drawBoard(true);

            // Receive message from the client
            char buffer[4096];
            memset(buffer, 0, sizeof(buffer));
            ssize_t bytesRead = recv(newClientSocket, buffer, sizeof(buffer), 0);
            if (bytesRead < 0) {
                cerr << "Error al recibir el mensaje." << endl;
                return;
            }
            cout << "Mensaje del cliente: " << buffer << endl;

            // Send message to the client
            string message = "Hola desde el servidor";
            ssize_t bytesSent = send(newClientSocket, message.c_str(), message.size(), 0);
            if (bytesSent < 0) {
                cerr << "Error al enviar el mensaje." << endl;
                return;
            }

            // Close the client socket
            close(newClientSocket);

            // Remove the client socket from the vector
            clientSockets.erase(remove(clientSockets.begin(), clientSockets.end(), newClientSocket), clientSockets.end());
        });

        // Detach the thread so it runs independently
        clientThread.detach();
    }

    // Close server socket
    close(serverSocket);

    return 0;
}