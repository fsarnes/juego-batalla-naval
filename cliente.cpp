/*
 * Compilación: g++ cliente.cpp -o cliente
 * Uso: ./cliente <ip> <puerto>
 */

#include <vector>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sys/time.h>
#include <limits.h>

using namespace std;

int getPort(char *argv[]) {
    char *p;
    errno = 0;

    long conv = strtol(argv[2], &p, 10);

    /* Valida que el puerto sea un entero válido */
    if (errno != 0 || *p != '\0' || conv > INT_MAX || conv < INT_MIN) {
        return false;
    } else {
        return conv;
    }
}

int main(int argc, char *argv[]) {
    // Create socket
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0) {
        cerr << "Error creating socket." << endl;
        return 1;
    }

    // Connect to the server
    struct sockaddr_in serverAddress{};
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(getPort(argv));
    if (inet_pton(AF_INET, argv[1], &(serverAddress.sin_addr)) <= 0) {
        cerr << "Dirección inválida/Dirección no soportada." << endl;
        return 1;
    }
    if (connect(clientSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        cerr << "Conexión fallida" << endl;
        return 1;
    }

    // Send multiple messages to the server
    vector<string> messages;
    string input;
    char buffer[1024];

    cout << "Ingresar mensajes a enviar (ingresar 'listo' para terminar):" << endl;
    while (true) {
        getline(cin, input);
        if (input == "listo")
            break;
        // Send message to the server
        ssize_t bytesSent = send(clientSocket, input.c_str(), input.size(), 0);
        memset(buffer, 0, sizeof(input));
        if (bytesSent < 0) {
            cerr << "Error al enviar el mensaje." << endl;
            break;
        }
    }

    close(clientSocket);

    return 0;
}