CC = g++
CFLAGS = -O3 -Wall
LIBS = -lpthread

all: servidor cliente

servidor: servidor.o
	$(CC) $(CFLAGS) -o servidor servidor.o $(LIBS)

cliente: cliente.o
	$(CC) $(CFLAGS) -o cliente cliente.o

servidor.o: servidor.cpp
	$(CC) $(CFLAGS) -c servidor.cpp

cliente.o: cliente.cpp
	$(CC) $(CFLAGS) -c cliente.cpp

clean:
	rm -f *.o servidor cliente
